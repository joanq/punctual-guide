import * as P from "./punctual.js";

window.footer = true;
window.info = false;

function PunctualInstance(container) {
    this.container = container;
    this.textarea = container.querySelector('textarea');
    this.errorElement = container.querySelector('.punctualError');
    this.punctual = new P.Punctual();
    this.timeOfLastFPSUpdate = Date.now() / 1000.0;
    this.framesSinceFPSUpdate = 0;
    this.isActive = false;

    this.animate = this.animate.bind(this);
    this.eval = this.eval.bind(this);

    this.textarea.addEventListener('keydown', (e) => {
        if (e.shiftKey && e.key == "Enter") {
            e.preventDefault();
            this.eval(this.textarea.value);
        }
    });

    this.eval(this.textarea.value);
}

PunctualInstance.prototype.animate = function () {
    if (this.isActive) {
        window.requestAnimationFrame(this.animate);
        var now = Date.now() / 1000.0;
        this.punctual.preRender({ canDraw: true, nowTime: now });
        this.punctual.render({ canDraw: true, zone: 0, nowTime: now });
        this.punctual.postRender({ canDraw: true, nowTime: now });
        this.framesSinceFPSUpdate += 1;
    }
};

PunctualInstance.prototype.eval = function (t) {
    this.punctual.define({ zone: 0, text: t, time: Date.now() / 1000.0 })
        .then(r => {
            const punctualCanvas = document.querySelector('body > canvas');
            if (punctualCanvas) {
                if (this.container) {
                    this.container.appendChild(punctualCanvas);
                }
            }
            this.errorElement.textContent = "";
        })
        .catch(e => {
            var eString = e.toString();
            this.errorElement.textContent = eString;
        });
};

PunctualInstance.prototype.stop = function () {
    this.isActive = false;
    this.punctual = null;
    this.container = null;
    this.textarea = null;
    console.log('PunctualInstance destroyed');
};

PunctualInstance.prototype.start = function () {
    if (!this.isActive) {
        this.isActive = true;
        window.requestAnimationFrame(this.animate);
    }
};

// Intersection Observer to observe editorAndStatus elements
const observer = new IntersectionObserver((entries) => {
    entries.forEach(entry => {
        const container = entry.target;
        if (entry.isIntersecting) {
            if (!entry.target.punctualInstance) {
                entry.target.punctualInstance = new PunctualInstance(container);
                console.log('PunctualInstance created');
            }
            entry.target.punctualInstance.start();
            console.log('PunctualInstance started');
        } else {
            if (entry.target.punctualInstance) {
                entry.target.punctualInstance.stop();
                entry.target.punctualInstance = null;
            }
        }
    });
});

// Observe all editorAndStatus elements
document.querySelectorAll('.punctualExample').forEach(container => {
    observer.observe(container);
});
