---
title: Punctual Guide
---

{{< blocks/cover title="Welcome to the complete guide to live-coding visuals in Punctual!" image_anchor="top" height="full" >}}
<div id="backgroundPunctualExample" class="punctualExample">
    <div class="editor">
        <textarea class="editorArea">a << spin (saw 0.004) $ ft;
a1 << spin (osc 0.002) $ tilexy [2,1] $ spin (saw 0.2) $ a*[spr [0.8,0.2,0.4] $ saw 0.2, 0.5, 0];
b << spin ([-1,1]*saw 0.013) $ tilexy [4,2] $ spin 1 $ a * [0.5, 0, spr [0.5,1,0.7] $ saw 0.3];
c << (a1 +: b) * 0.2;
move [-1,0] $ setfxy [5*fr*fx, 8*ft*fy] c >> add;
        </textarea>
    </div>
    <div class="punctualError"></div>
</div>
<a class="btn btn-lg btn-primary me-3 mb-4" href="/docs/">
  Learn More <i class="fas fa-arrow-alt-circle-right ms-2"></i>
</a>
{{< blocks/link-down color="info" >}}
{{< /blocks/cover >}}



{{% blocks/lead color="primary" %}}
Punctual is a language for live coding audio and visuals. Learn all the details of making visuals with this guide.
{{% /blocks/lead %}}


{{% blocks/section color="dark" type="row" %}}

{{% blocks/feature icon="fab fa-gitlab" title="Contributions welcome!" url="https://gitlab.com/joanq/punctual-guide" %}}
We do a [Merge Request](https://gitlab.com/joanq/punctual-guide/-/merge_requests) contributions workflow on **GitLab**. New users are always welcome!
{{% /blocks/feature %}}

{{% blocks/feature icon="fab fa-mastodon" title="Follow me on Mastodon" url="https://social.toplap.org/@savamala" rel="me" %}}
{{% /blocks/feature %}}

{{% /blocks/section %}}
