---
title: Overview
description: About Punctual and this guide
weight: 1
---

## About Punctual

From the [official Punctual repository](https://github.com/dktr0/Punctual):

> Punctual is a language for live coding audio and visuals. It allows you to build and change networks of signal processors (oscillators, filters, etc) on the fly. When definitions are changed, when and how they change can be explicitly indicated.

Punctual has huge visual capacities. Its strong points are:

- Runs in a browser, no installation needed.
- Fully integrated into the Estuary collaborative live-coding environment.
- Compact syntax allows fast pattern creation and further modification.
- Direct access to pixel coordinates allows the creation of patterns using any mathematical formula.
- Low-level geometry changing functions lead to a great flexibility.
- Flexible graphs arithmetic for even more creative possibilities.
- Simple but effective modulation functions. Modulate everything.
- Audio reactive visuals using frequency analysis, FFT (Fast Fourier Transform) and internal tempo.
- Feedback allows building complex patterns using the last frame as source.
- Capacity to use remote images and videos.
- Can use webcam as source.
- Easy to extend with your own Punctual functions.
- Easy to get help via the [Estuary discord server](https://discord.gg/E9vuAUBAeW).

Compared to [Hydra](https://hydra.ojack.xyz/) (arguably the best known live-coding language for visuals), it has the following limitations:

- Lack of high-level effects: saturation, pixelation, etc. (but many of them can be created using user functions).
- Some mathematical ability is needed to build complex patterns using the low-level functions Punctual provides.
- Not very well documented (until now, I hope).
- Not many people using it.

## About Estuary

From the [official Estuary repository](https://github.com/dktr0/estuary):

> Estuary is a platform for collaboration and learning through live coding. It enables you to create sound, music, and visuals in a web browser. Key features include:
    - built-in tutorials and reference materials
    - a growing collection of different interfaces and live coding languages
    - support for networked ensembles (whether in the same room or distributed around the world)
    - text localization to an expanding set of natural languages
    - visual customization via themes (described by CSS)

This guide is not about Estuary, but Estuary is important because it's one of the easiest and most feature rich ways to use Punctual. As the author of Punctual is also the main author of Estuary, it has complete support and it's always up-to-date.

## About this guide

I decided to write this guide after a year of participating on a [weekly jam at the Estuary platform](https://www.youtube.com/watch?v=bQjTJcSeiHA&list=PLMBIpibV-wQLvP7jitjnV9E61DfV11235), and using Punctual in these jams and also several times in live performances.

This guide deals only with the visuals part of Punctual and tries to be deep, documenting and exemplifying each of the functions in Punctual. For a gentle introduction to Punctual, check the [Tutorial section](/docs/tutorial) or the [Decoded workshop](https://decoded.livecode.au/#/).

Punctual is a somewhat low-level live-coding language, and has a very brief official documentation. While learning it, I always missed some more explanations and examples on how to use the distinct features the language provides. With this, I'm trying to write the documentation I had liked to find when I was learning to use Punctual.

Many of the examples presented here are the result of conversations in the Discord's Estuary server with **David Ogborn** (Punctual's author, who is extremely helpful and always answers my questions), and **Bernard Gray** (who introduced me to the weekly jams and has been my partner in this journey).

I'm an IT teacher with more than 20 years of experience, and have written a lot of documentation and tutorials for my students as well as many contributions in official product documentations, for example TidalCycles. Most of these are written in Catalan, my mother-tongue.

## Disclaimer

Punctual is a personal art project by **David Ogborn** (*@dktr0*). He likes to keep absolute freedom on how or when Punctual evolve, and that's the main reason why he doesn't usually accept contributions into the source code or the official documentation.

This is an unofficial document and can be made obsolete by changes on Punctual at any time, even though I'll try to keep it up to date. This is specially true for any officially undocumented feature that may appear here.

The only official documentation is maintained by David himself on the [Punctual git repository](https://github.com/dktr0/Punctual). Make sure to check the `README.md` and `REFERENCE.md` files for up-to-date, official information on the project.

This guide was last updated for Punctual version `0.5.1.1`.

## License and contributions

This guide is licensed under the terms of the [Creative Commons Share Alike license](https://creativecommons.org/licenses/by-sa/4.0/).

Contributions that updates content or add interesting examples are very welcomed.
