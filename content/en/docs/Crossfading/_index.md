---
title: Cross-fading
description: Adjusting the transition time between two expressions
weight: 14
---

When introducing a new expression, Punctual always waits to the start of the next cycle to execute it. At that point, a transition occurs between the last expression and the new one. By default, this transition is quite fast, but the duration of the fade from one expression to another is adjustable.

### `<>`

To control the transition time, you can use the <> operator at the end of the expression. The number following the operator specifies the duration of the fade. For example, the following code will fade from the current pattern to a new one over a period of 2 seconds:

```haskell
[1,0,0] >> add <> 2;
```

{{< punctualExample >}}
[1,0,0] >> add <> 2;
{{< /punctualExample >}}

Try changing the color components and the duration of the fade to see how the transition time affects the visual output.

It's important to note that specifying 0 for cross-fading will result in an instantaneous change at the beginning of the next cycle.

Interestingly, Punctual accepts negative numbers for the transition time, and depending on the specified time, it causes a transition from the new pattern back to the old one.

When using multiple output notations, each expression will fade at its own pace:

```haskell
[fx>0,0,0] >> add <> 2;
[fx<0,0,0] >> add <> 0.5;
```

{{< punctualExample >}}
[fx>0,0,0] >> add <> 2;
[fx<0,0,0] >> add <> 0.5;
{{< /punctualExample >}}

In this example, the first expression will fade over a duration of 2 seconds, while the second expression will have a quicker fade lasting 0.5 seconds. This flexibility allows for distinct transition times between different visual elements.

Lastly, it's worth mentioning that the `zero` function can be very useful for fading out a pattern, especially towards the end of a performance. You can use `zero $` before any pattern, set a time for the transition, and gradually fade out the current pattern to black.
