---
title: About Punctual Guide
linkTitle: About
menu: {main: {weight: 10}}
---

{{% blocks/cover title="About Punctual Guide" image_anchor="bottom" height="auto" %}}

The complete guide to live-coding visuals in Punctual.
{.mt-5}

{{% /blocks/cover %}}

{{% blocks/lead %}}

Punctual is a language for live coding audio and visuals. Even though it is a quite unknown language, it has great potential for the creative generation of visual effects. Moreover, it is completely integrated into Estuary, an online collaborative platform.

This guide deals only with the visuals part of Punctual and tries to be
deep, documenting and exemplifying each of the functions in Punctual. For a more
gentle introduction to Punctual.

{{% /blocks/lead %}}
